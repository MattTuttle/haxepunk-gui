package world;

import com.haxepunk.gui.Button;
import com.haxepunk.gui.Carousel;
import com.haxepunk.gui.CheckBox;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.MenuItem;
import com.haxepunk.gui.MenuList;
import com.haxepunk.gui.Panel;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.gui.tests.ButtonTestWorld;
import com.haxepunk.gui.tests.LabelTestWorld;
import com.haxepunk.gui.tests.ListTestWorld;
import com.haxepunk.gui.Window;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Scene;
import flash.events.Event;
import nme.text.TextFormatAlign;

/**
 * ...
 * @author Samuel Bouchet
 */

class WelcomeScene extends Scene
{
	public static var instance:Scene ;

	public function new(skin:String = "gfx/ui/greyDefault.png")
	{
		super();
		// Choose custom skin. Parameter can be a String to resource or a bitmapData.
		Control.useSkin(skin);
		// The default layer where every component will be displayed on.
		// Most components use severals layers (at least 1 per component child). A child component layer will be <100.
		Control.defaultLayer = 100;
		// Use this to fit your button skin's borders, set the default padding of every new Button and ToggleButton.
		// padding attribute can be changed on instances after creation.
		Button.defaultPadding = 4;
		// Size in px of the tickBox for CheckBoxes. Default is skin native size : 12.
		CheckBox.defaultBoxSize = 12;
		// Same for RadioButtons.
		RadioButton.defaultBoxSize = 12;
		// Label defaults parameters affect every components that uses labels : Button, ToggleButton, CheckBox, RadioButton, MenuItem, Window Title.
		// Those labels are always accessible using "myComponent.label" and you can change specific Labels apperence any time.
		// Label default font (must be a nme.text.Font object).
		Label.defaultFont = nme.Assets.getFont("font/pf_ronda_seven.ttf");
		// Label defaultColor. Tip inFlashDevelop : use ctrl + shift + k to pick a color.
		Label.defaultColor = 0x1E4E82;
		// Label default Size.
		Label.defaultSize = 8;

		var menu:MenuList = new MenuList();

		var b:MenuItem;
		menu.addControl(b = new MenuItem("Test Labels",null,0,0,0,60,24));
		menu.addControl(b = new MenuItem("Test Buttons",null,0,0,0,60,24));
		menu.addControl(b = new MenuItem("Test Lists",null,0,0,0,60,24));

		menu.localX = Math.round(HXP.screen.width/(2*HXP.screen.scale)-menu.halfWidth);
		menu.localY = Math.round(HXP.screen.height / (2 * HXP.screen.scale) - menu.halfHeight);
		menu.addEventListener(Button.CLICKED, goto);
		add(menu);

		var title:Label = new Label("HaXePunk GUI - Demo", 0, 0, Math.round(HXP.screen.width / (HXP.screen.scale)), 60, TextFormatAlign.CENTER);
		title.size = 16;
		title.color = 0x5A96EF;
		add(title);


		instance = this;
	}

	private function goto(e:ControlEvent):Void
	{
		var list:MenuList = cast(e.control, MenuList);
		if (list != null) {
			var item:MenuItem = cast(list.selectedItem, MenuItem);
			if(item!=null){
				switch(item.label.text) {
					case "Test Labels":
						gotoLabeltest();
					case "Test Buttons":
						gotoButtontest();
					case "Test Lists":
						gotoListtest();
				}
			}
		}
	}

	override public function update():Dynamic
	{
		super.update();

		var skin:String = "";
		if (Input.pressed(Key.NUMPAD_0)) {
			skin = "gfx/ui/greyDefault.png";
		}
		if (Input.pressed(Key.NUMPAD_1)) {
			skin = "gfx/ui/greenMagda.png";
		}
		if (Input.pressed(Key.NUMPAD_2)) {
			skin = "gfx/ui/blueMagda.png";
		}
		if (Input.pressed(Key.NUMPAD_3)) {
			skin = "gfx/ui/purpleMagda.png";
		}
		if (skin != "") {
			HXP.scene = new WelcomeScene(skin);
		}

	}

	private function gotoListtest(?e:Event):Void
	{
		HXP.scene = new ListTestWorld(this);
	}

	private function gotoButtontest(?e:Event):Void
	{
		HXP.scene = new ButtonTestWorld(this);
	}

	private function gotoLabeltest(?e:Event):Void
	{
		HXP.scene = new LabelTestWorld(this);
	}

	override public function begin():Dynamic
	{
		super.begin();
	}

	override public function end():Dynamic
	{
		super.end();
	}

}