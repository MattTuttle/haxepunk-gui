package world;

import com.haxepunk.gui.Button;
import com.haxepunk.gui.Carousel;
import com.haxepunk.gui.CheckBox;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.event.ControlEvent;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.MenuItem;
import com.haxepunk.gui.MenuList;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Scene;
import demo.ButtonDemoPanel;
import demo.CarouselDemoPanel;
import demo.CheckBoxDemoPanel;
import demo.MenuListDemoPanel;
import demo.PanelWindowDemoPanel;
import demo.RadioButtonDemoPanel;
import nme.events.Event;

/**
 * ...
 * @author Samuel Bouchet
 */

class DemoScene extends Scene
{
	public static var instance:DemoScene ;
	private var screenWidth:Int;
	private var screenHeight:Int;
	public var menu:Carousel;

	private var moveDelay:Int = 5;

	public function new(skin:String = "gfx/ui/greyDefault.png", menuIndex:Int = 0 )
	{
		super();
		initHaxePunkGui(skin);

		screenWidth = Math.round(HXP.screen.width / (HXP.screen.scale));
		screenHeight = Math.round(HXP.screen.height / (HXP.screen.scale));

		menu = new Carousel(-Math.round(screenWidth*2/2)+ Math.round(screenWidth/2), -Math.round(screenHeight/2) +Math.round(screenHeight/2),  Math.round(screenWidth*2), Math.round(screenHeight/2));
		menu.layer = 5000;
		menu.nonFixedRotationDurationInSeconds = 0.01;
		menu.fixedRotationDurationInFrames = 1;
		menu.addControl(new ButtonDemoPanel(screenWidth, screenHeight));
		menu.addControl(new RadioButtonDemoPanel(screenWidth, screenHeight));
		menu.addControl(new CheckBoxDemoPanel(screenWidth, screenHeight));
		menu.addControl(new PanelWindowDemoPanel(screenWidth, screenHeight));
		menu.addControl(new CarouselDemoPanel(screenWidth, screenHeight));
		menu.addControl(new MenuListDemoPanel(screenWidth, screenHeight));
		menu.selectedId = menuIndex;
		menu.nonFixedRotationDurationInSeconds = 0.4;
		menu.fixedRotationDurationInFrames = 30;
		menu.allowDragToRoll = false;
		menu.followCamera = true;
		add(menu);

		var text:String = "I'm stuck on the world and the camera is moving !\n" +
						  "But others Controls have followCamera=true so we don't even notice it !";
		var noFollowTest:Label = new Label(text, 150, 150);
		noFollowTest.layer = 60;
		noFollowTest.color = 0xFFFFFF;
		add(noFollowTest);
		var noFollowTestShadow:Label = new Label(text, 150, 151);
		noFollowTestShadow.layer = 61;
		noFollowTestShadow.color = 0x000000;
		add(noFollowTestShadow);
		var noFollowTestShadow2:Label = new Label(text, 150, 149);
		noFollowTestShadow2.layer = 61;
		noFollowTestShadow2.color = 0x000000;
		add(noFollowTestShadow2);
		var noFollowTestShadow3:Label = new Label(text, 151, 150);
		noFollowTestShadow3.layer = 61;
		noFollowTestShadow3.color = 0x000000;
		add(noFollowTestShadow3);
		var noFollowTestShadow4:Label = new Label(text, 149, 150);
		noFollowTestShadow4.layer = 61;
		noFollowTestShadow4.color = 0x000000;
		add(noFollowTestShadow4);

		instance = this;
	}

	override public function update()
	{
		moveDelay--;
		if (moveDelay <= 0){
			HXP.camera.x += 1;
			HXP.camera.y += 1;
			moveDelay += 5;
		}

		var skin:String = "";
		if (Input.pressed(Key.NUMPAD_0)) {
			skin = "gfx/ui/greyDefault.png";
		}
		if (Input.pressed(Key.NUMPAD_1)) {
			skin = "gfx/ui/greenMagda.png";
		}
		if (Input.pressed(Key.NUMPAD_2)) {
			skin = "gfx/ui/blueMagda.png";
		}
		if (Input.pressed(Key.NUMPAD_3)) {
			skin = "gfx/ui/purpleMagda.png";
		}
		if (Input.pressed(Key.LEFT)) {
			menu.selectPrevious();
		}
		if (Input.pressed(Key.RIGHT)) {
			menu.selectNext();
		}
		if (skin != "") {
			HXP.scene = new DemoScene(skin, menu.selectedId);
		}

		super.update();
	}

	private function initHaxePunkGui(skin:String):Void
	{
		// Choose custom skin. Parameter can be a String to resource or a bitmapData.
		Control.useSkin(skin);
		// The default layer where every component will be displayed on.
		// Most components use severals layers (at least 1 per component child). A child component layer will be <100.
		Control.defaultLayer = 100;
		// Use this to fit your button skin's borders, set the default padding of every new Button and ToggleButton.
		// padding attribute can be changed on instances after creation.
		Button.defaultPadding = 4;
		// Size in px of the tickBox for CheckBoxes. Default is skin native size : 12.
		CheckBox.defaultBoxSize = 12;
		// Same for RadioButtons.
		RadioButton.defaultBoxSize = 12;
		// Label defaults parameters affect every components that uses labels : Button, ToggleButton, CheckBox, RadioButton, MenuItem, Window Title.
		// Those labels are always accessible using "myComponent.label" and you can change specific Labels apperence any time.
		// Label default font (must be a nme.text.Font object).
		Label.defaultFont = nme.Assets.getFont("font/pf_ronda_seven.ttf");
		// Label defaultColor. Tip inFlashDevelop : use ctrl + shift + k to pick a color.
		Label.defaultColor = 0x1E4E82;
		// Label default Size.
		Label.defaultSize = 8;
	}

	override public function begin()
	{
		super.begin();
	}

	override public function end()
	{
		removeAll();
		super.end();
	}

}