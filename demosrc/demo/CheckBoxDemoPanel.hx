package demo;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.CheckBox;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.gui.ToggleButton;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import flash.events.Event;
import nme.Assets;
import nme.geom.Point;
import nme.text.TextFormatAlign;

/**
 * ...
 * @author Lythom
 */

class CheckBoxDemoPanel extends DemoPanel
{
	
	private var buttons:Array<CheckBox> ;
	private var g1Label:Label;
	private var g2Label:Label;

	public function new(width:Int, height:Int)
	{
		super(0, 0, width, height, true);
		
		buttons = new Array<CheckBox>();
		
		var cursor:Point = new Point(10, 10);
		var margin:Int = 8;
		
		// place title
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		var l:Label = new Label("CheckBox Demo", cursor.x, cursor.y, Math.round(width - cursor.x));
		l.align = TextFormatAlign.CENTER;
		l.size = 16;
		l.color = 0x000000;
		addControl(l);
		
		// 1st group of CheckBox
		CheckBox.defaultBoxSize = 12;
		
		cursor.y += margin + l.height;
		var b:Button = new CheckBox("standard checkbox", false, cursor.x, cursor.y);
		addControl(b);
		
		cursor.y += margin + b.height;
		b =  new CheckBox("checked checkbox", true, cursor.x, cursor.y);
		addControl(b);
		
		cursor.y += margin + b.height;
		b =  new CheckBox("colored checkbox", false, cursor.x, cursor.y);
		b.color = 0x731751;
		addControl(b);
		
		cursor.y += margin + b.height;
		b =  new CheckBox("colored checkbox 2", false, cursor.x, cursor.y);
		b.color = 0xA46200;
		addControl(b);
		
		// 2nd group of radio
		CheckBox.defaultBoxSize = 0;
		
		cursor.y += margin + b.height;
		var b:Button = new CheckBox("resizable box checkbox", false, cursor.x, cursor.y);
		addControl(b);
		
		cursor.y += margin + b.height;
		b =  new CheckBox("checked checkbox", true, cursor.x, cursor.y);
		addControl(b);
		
		cursor.y += margin + b.height;
		b =  new CheckBox("colored checkbox", false, cursor.x, cursor.y);
		b.color = 0x731751;
		addControl(b);
		
		cursor.y += margin + b.height;
		b =  new CheckBox("colored checkbox 2", false, cursor.x, cursor.y);
		b.color = 0xA46200;
		addControl(b);
		
		cursor.y = globalInstructionLabel.y + globalInstructionLabel.height + margin;
		cursor.x = globalInstructionLabel.x;
		var instructions:String = "Specific Instructions :\n";
		instructions += "+ - increase text size\n";
		instructions += "- - decrease text size\n";
		instructions += "f - switch font family\n";
		instructions += "d - enable/disable buttons\n";
		l = new Label(instructions, cursor.x, cursor.y);
		addControl(l);
		
	}
	
	override public function addControl(child:Control, ?position:Int):Void
	{
		if (Std.is(child, CheckBox)) {
			buttons.push(cast(child,CheckBox));
		}
		super.addControl(child, position);
	}
	
	override public function update()
	{
		if (!enabled) {
			return;
		}
		
		if (Input.pressed(Key.NUMPAD_ADD)) {
			for (b in buttons) {
				b.label.size++;
			}
		}
		if (Input.pressed(Key.NUMPAD_SUBTRACT)) {
			for (b in buttons) {
				b.label.size--;
			}
		}
		if (Input.pressed(Key.F)) {
			for (b in buttons) {
				if (b.font == Assets.getFont("font/pf_ronda_seven.ttf").fontName) {
					b.font = Assets.getFont("font/04B_03__.ttf").fontName;
				} else {
					b.font = Assets.getFont("font/pf_ronda_seven.ttf").fontName;
				}
			}
		}
		if (Input.pressed(Key.D)) {
			for (b in buttons) {
				b.enabled = !b.enabled;
			}
		}
		super.update();
	}
	
}