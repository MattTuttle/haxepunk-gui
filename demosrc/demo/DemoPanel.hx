package demo;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import com.haxepunk.gui.ToggleButton;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import nme.geom.Point;
import nme.text.TextFormatAlign;
import world.DemoScene;

/**
 * ...
 * @author Lythom
 */

class DemoPanel extends Panel
{
	private var globalInstructionLabel:Label;

	public function new(x:Float = 0, y:Float = 0, width:Int = 1, height:Int = 1, displayBackground:Bool=true)
	{
		super(0, 0, width, height, true);
		globalInstructionLabel = new Label();
		globalInstructionLabel.text = "General Instructions :\n";
		globalInstructionLabel.text += "0 - Use default skin\n";
		globalInstructionLabel.text += "1 - Use greenMagda skin\n";
		globalInstructionLabel.text += "2 - Use blueMagda skin\n";
		globalInstructionLabel.text += "3 - Use purpleMagda skin\n";
		globalInstructionLabel.text += "4 - Button and ToggleButton\n";
		globalInstructionLabel.text += "5 - RadioButton\n";
		globalInstructionLabel.text += "6 - CheckBox\n";
		globalInstructionLabel.text += "7 - Panel and Window\n";
		globalInstructionLabel.text += "8 - Carousel\n";
		globalInstructionLabel.text += "9 - MenuList";
		globalInstructionLabel.localX = 10;
		globalInstructionLabel.localY = 10;
		addControl(globalInstructionLabel);
	}

	override public function update()
	{
		if (Input.pressed(Key.NUMPAD_4)) {
			DemoScene.instance.menu.selectedId = 0;
		}
		if (Input.pressed(Key.NUMPAD_5)) {
			DemoScene.instance.menu.selectedId = 1;
		}
		if (Input.pressed(Key.NUMPAD_6)) {
			DemoScene.instance.menu.selectedId = 2;
		}
		if (Input.pressed(Key.NUMPAD_7)) {
			DemoScene.instance.menu.selectedId = 3;
		}
		if (Input.pressed(Key.NUMPAD_8)) {
			DemoScene.instance.menu.selectedId = 4;
		}
		if (Input.pressed(Key.NUMPAD_9)) {
			DemoScene.instance.menu.selectedId = 5;
		}
		super.update();
	}

}