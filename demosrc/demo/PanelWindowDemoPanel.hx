package demo;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import com.haxepunk.gui.ToggleButton;
import com.haxepunk.gui.Window;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import nme.Assets;
import nme.geom.Point;
import nme.text.TextFormatAlign;

/**
 * ...
 * @author Lythom
 */

class PanelWindowDemoPanel extends DemoPanel
{

	private var buttons:Array<Button> ;
	private var moving:Bool;
	private var angle:Float;
	private var progress:Int;
	private var p1:Panel;
	private var p2:Panel;
	private var p3:Panel;
	private var p4:Panel;
	private var initialized:Bool;

	public function new(width:Int, height:Int)
	{
		super(0, 0, width, height, true);

		buttons = new Array<Button>();

		var cursor:Point = new Point(10, 10);
		var margin:Int = 10;

		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		var l:Label = new Label("Panel and Window", cursor.x, cursor.y, Math.round(width - cursor.x));
		l.align = TextFormatAlign.CENTER;
		l.size = 16;
		l.color = 0x000000;
		addControl(l);

		cursor.y += margin + l.height;
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		p1 = new Panel(cursor.x, cursor.y, 70, 70, true);

		cursor.x += p1.width + 50;
		p2 = new Panel(cursor.x, cursor.y, 70, 70, true);

		cursor.y += p2.height + 50;
		p3 = new Panel(cursor.x, cursor.y, 70, 70, false);

		cursor.y = p3.y;
		cursor.x = p1.x;
		p4 = new Window("Window 1", cursor.x, cursor.y, 70, 70);

		addControl(p1);
		addControl(p2);
		addControl(p3);
		//addControl(p4);

		p1.layer = 90;
		p1.addControl(new Label("Panel 1", 0, 4, p1.width, 0, TextFormatAlign.CENTER));
		p1.addControl(new Button("B1", 5, 35));
		p1.addControl(new Button("B2", 35, 35));
		p1.updateChildPosition();

		p2.layer = 80;
		p2.addControl(new Label("Panel 2", 0, 4, p1.width, 0, TextFormatAlign.CENTER));
		p2.addControl(new Button("B1", 5, 35));
		p2.addControl(new Button("B2", 35, 35));
		p2.updateChildPosition();

		p3.layer = 70;
		p3.addControl(new Label("Panel 3", 0, 4, p1.width, 0, TextFormatAlign.CENTER));
		p3.addControl(new Button("B1", 5, 35));
		p3.addControl(new Button("B2", 35, 35));
		p3.updateChildPosition();

		p4.layer = 60;
		p4.addControl(new Label("Panel 4", 0, 4, p1.width, 0, TextFormatAlign.CENTER));
		p4.addControl(new Button("B1", 5, 35));
		p4.addControl(new Button("B2", 35, 35));
		p4.updateChildPosition();

		angle = 0;
		progress = 0;
		initialized = false;
		moving = false;

		cursor.y = globalInstructionLabel.y + globalInstructionLabel.height + margin;
		cursor.x = globalInstructionLabel.x;
		var instructions:String = "Specific Instructions :\n";
		instructions += "+ - move panels\n";
		instructions += "- - stop panels\n";
		instructions += "click&drag window title\n";
		l = new Label(instructions, cursor.x, cursor.y);
		addControl(l);

	}

	override public function added()
	{
		super.added();
		// test Window in a world condition
		//p4.followCamera = true;
		scene.add(p4);
	}

	override public function addControl(child:Control, ?position:Int):Void
	{
		if (Std.is(child, Button)) {
			buttons.push(cast(child,Button));
		}
		super.addControl(child, position);
	}

	override public function update()
	{
		if (!enabled) {
			return;
		}
		if (p4.layer != 50) {
			p1.layer = 80;
			p2.layer = 70;
			p3.layer = 60;
			p4.layer = 50;
		}

		if (Input.pressed(Key.NUMPAD_ADD)) {
			moving = true;
		}
		if (Input.pressed(Key.NUMPAD_SUBTRACT)) {
			moving = false;
		}

		if (moving) {
			p1.localX += Math.cos(angle);
			p1.localY += Math.sin(angle);
			p2.localX += Math.cos(angle+Math.PI / 2);
			p2.localY += Math.sin(angle+Math.PI / 2);
			p3.localX += Math.cos(angle+Math.PI);
			p3.localY += Math.sin(angle+Math.PI);
			p4.localX += Math.cos(angle-Math.PI/2);
			p4.localY += Math.sin(angle-Math.PI/2);
			progress ++;
			if (progress == 120) {
				progress = 0;
				angle = (angle + Math.PI / 2) % (2 * Math.PI);
			}
		}
		super.update();
	}

}