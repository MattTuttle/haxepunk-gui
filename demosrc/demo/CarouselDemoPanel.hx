package demo;
import com.haxepunk.graphics.Image;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Carousel;
import com.haxepunk.gui.CheckBox;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import com.haxepunk.gui.RadioButton;
import com.haxepunk.gui.ToggleButton;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import nme.Assets;
import nme.geom.Point;
import nme.text.TextFormatAlign;

/**
 * ...
 * @author Lythom
 */

class CarouselDemoPanel extends DemoPanel
{
	
	private var buttons:Array<Button> ;
	private var car:Carousel;

	public function new(width:Int, height:Int)
	{
		super(0, 0, width, height, true);
		
		buttons = new Array<Button>();
		
		var cursor:Point = new Point(10, 10);
		var margin:Int = 10;
		
		// title
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		var l:Label = new Label("Carousel", cursor.x, cursor.y, Math.round(width - cursor.x));
		l.align = TextFormatAlign.CENTER;
		l.size = 16;
		l.color = 0x000000;
		addControl(l);
		
		// Carousel
		cursor.y += margin + l.height;
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		car = new Carousel(cursor.x + 50, cursor.y+50, 200, 100);
		
		// Carousel controls
		var c:Control = new Control();
		c.graphic = new Image(Control.currentSkin);
		c.width = cast(c.graphic, Image).width;
		c.height = cast(c.graphic, Image).height;
		car.addControl(c);
		
		var b:Button = new Button("Button");
		car.addControl(b);
		
		var tb:ToggleButton = new ToggleButton("Toggle button");
		car.addControl(tb);
		
		var cb:CheckBox = new CheckBox("Checkbox");
		car.addControl(cb);
		
		var rbp:Panel = new Panel(0, 0, 80, 60);
		for (i in 0...3) 
		{
			var rb:RadioButton  = new RadioButton("groupCar", "Radio g1 " + i, "v" + i, 5, 0);
			rb.localY = 5 + i * 18;
			rbp.addControl(rb);
		}
		car.addControl(rbp);
		
		addControl(car);
		
		// instructions
		cursor.y = globalInstructionLabel.y + globalInstructionLabel.height + margin;
		cursor.x = globalInstructionLabel.x;
		var instructions:String = "Specific Instructions :\n";
		instructions += "up arrow - Spin right\n";
		instructions += "down arrow - Spin left\n";
		instructions += "drag from center - Free spin\n";
		l = new Label(instructions, cursor.x, cursor.y);
		addControl(l);
		
		cursor.y = l.y + l.height + margin;
		cursor.x = l.x;
		instructions = "This whole demo is a set of Panels\n";
		instructions += "added in a Carousel. You can select\n";
		instructions += "directly one element. Try numpad\n";
		instructions += "numbers 4 to 9 to test it.";
		l = new Label(instructions, cursor.x, cursor.y);
		addControl(l);
		
	}
	
	override public function addControl(child:Control, ?position:Int):Void
	{
		if (Std.is(child, Button)) {
			buttons.push(cast(child,Button));
		}
		super.addControl(child, position);
	}
	
	override public function update()
	{
		if (!enabled) {
			return;
		}
		
		if (Input.pressed(Key.UP)) {
			car.selectNext();
		}
		if (Input.pressed(Key.DOWN)) {
			car.selectPrevious();
		}
	
		super.update();
	}
	
}